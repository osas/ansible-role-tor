Ansible role to install tor.

This role just pull and install tor from package by default.
It is used by others role to support hidden services. For example,
on https://gitlab.com/osas/ansible-role-httpd have a setting 
`use_hidden_service`.

# Deploying a relay

If you wish to deploy a relay on tor, you can do it this way:

```
- hosts: torrelay
  roles:
  - role: tor
    enable_relay: True
    # in megabytes
    relay_bandwidth: '500'   
```

# Reusing the role for hidden/onion services

Tor have a concept of hidden services, also called onion services (the whole concept
is being rebranded at the moment (2017), and is explained on
https://www.torproject.org/docs/hidden-services.html.en

If you want to offer it as a option for a role, the easiest way to do so
is to add it to meta/main.yml like this:

```
dependencies:
  - role: tor
    enable_onion_services: True
    when: "use_onion_service == True"
```

Then, the role offering the hidden/onion service can just be called with the
`use_onion_service` variable to activate it, and drop the required configuration with a task like
this (for example, for the port 22):

```
- name: Add the ssh HiddenServiceDir config to torrc
  blockinfile:
    dest: "{{ torrc }}"
    block: |
      HiddenServiceDir {{ tordir }}/onion_service_ssh
      HiddenServicePort 22 127.0.0.1:22
  notify: restart tor

```

The variables `torrc` and `tordir` will come from the execution of tor role, as well as the handler.

## Variables and defaults values for relay

### Relay Port

To change the default port from 9001 to something else, you can pass the variable `relay_port`.
Future version of the role will also take care of the firewall, but that's not the case for now.

### Nickname

Nickname is by default set to the hostname of the relay. If you wish to change it, just use
`nickname` when declaring the role.
